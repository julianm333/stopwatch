import java.util.Arrays;




public class insertionSort {
    public static void main(String args[]) {
        Stopwatch stopwatch = new Stopwatch();
	int size = 40000;
	int[] arr = new int[size];
	arr = generateArr(size);
	stopwatch.start();
	sort(arr);
	System.out.println(stopwatch.stop());
    }

    public static int[] generateArr(int k) {
	int[] arr2 = new int[k];
	for (int i = 0; i < k; i++) {
	    arr2[i] = (int)(Math.random() * 10000);
	}
	return arr2;
    }

    public static void sort(int[] arr) {
	for (int j = 1; j < arr.length - 2; j++) {
	    int x = j;
	    for (int i = j + 1; i < arr.length - 1; i++) {
		if (arr[i] < arr[x]) {
		    x = i;
		}
	    }
	    if (x != j) {
		int temp = arr[x];
		arr[x] = arr[j];
		arr[j] = temp;
	    }
	}
    }
}


